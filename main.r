install.packages("googleAnalyticsR", dependencies = TRUE)

# TODO : Use .Renviron file instead of hardcoded value
#Setting client.id from gar_auth_configure(path = json)
googleAuthR::gar_set_client("<ADD_YOUR_JSON_CLIENT_ID_FILE_PATH_HERE>")
library(googleAnalyticsR)
ga_auth()

## View account_list and pick the viewId you want to extract data from
ga_id <- 154884107


## for date range we can use dynamic dates, start_date <- "60daysAgo" end_date <- "yesterday" example
## simple query to test connection
eight_month_date_range <- c("2020-01-20", "2020-09-11")
one_month_date_range <- c("2020-08-20", "2020-09-18")

# anti_sample + max
genderReachForRadio1 <- google_analytics(ga_id,
                                       date_range = one_month_date_range,
                                       metrics = "users",
                                       dimensions = "userGender",
                                       filtersExpression = "ga:eventLabel==CHAA-FM",
                                       anti_sample = TRUE,
                                       max = -1)

# anti_sample + max
ageReachForRadio1 <- google_analytics(ga_id,
                                      date_range = one_month_date_range,
                                      metrics = "users",
                                      dimensions = "userAgeBracket",
                                      filtersExpression = "ga:eventLabel==CILM-FM",
                                      anti_sample = TRUE,
                                      max = -1)

## Multiple request (up to 5)
## First request we make via make_ga_4_req()
## Must have same date range
multidate_test <- make_ga_4_req(ga_id,
                                date_range = one_month_date_range,
                                metrics = "users",
                                dimensions = "userAgeBracket",
                                filtersExpression= "ga:eventLabel==CILM-FM",
                                samplingLevel = "LARGE")

## Second request - same date ranges and ID required, but different dimensions/metrics/order.
multi_test2 <- make_ga_4_req(ga_id,
                             date_range = one_month_date_range,
                             metrics = "users",
                             dimensions = "userAgeBracket",
                             filtersExpression= "ga:eventLabel==CFJU-FM",
                             samplingLevel = "LARGE")

## Request the two calls by wrapping them in a list() and passing to fetch_google_analytics()
ga_data3 <- fetch_google_analytics_4(list(multidate_test, multi_test2))
ga_data3

##### userAge
# Default (2 s)
# Data is sampled 
ageReachForRadio2 <- google_analytics(ga_id,
                                      date_range = eight_month_date_range,
                                      metrics = "users",
                                      dimensions = "userAgeBracket",
                                      filtersExpression = "ga:eventLabel==CILM-FM")


# With slow fetch (45 s)
# By default, 50,000 rows will be requested per API call. (10 000 rows x 5 calls)
# Reset to 10 000 rows per call
ageReachForRadio3 <- google_analytics(ga_id,
                                     date_range = eight_month_date_range,
                                     metrics = "users",
                                     dimensions = "userAgeBracket",
                                     filtersExpression = "ga:eventLabel==CILM-FM",
                                     slow_fetch = TRUE)

# With anti-smaple, longer, will fetch all rows (6 min 30 s)
# I got a 503 error, due to complicated queries (i.e. lots of segments or dimensions) or too big queries
# 2 times retry
# Avoided sampling
ageReachForRadio4 <- google_analytics(ga_id,
                                     date_range = eight_month_date_range,
                                     metrics = "users",
                                     dimensions = "userAgeBracket",
                                     filtersExpression = "ga:eventLabel==CILM-FM",
                                     anti_sample = TRUE)

##### userGender
# Default (25 s), more data
# Data is simpled 
genderReachForRadio2 <- google_analytics(ga_id,
                                      date_range = eight_month_date_range,
                                      metrics = "users",
                                      dimensions = "userGender",
                                      filtersExpression = "ga:eventLabel==CILM-FM")


# With slow fetch (2 s)
# By default, 50,000 rows will be requested per API call. (10 000 rows x 5 calls)
# Reset to 10 000 rows per call
genderReachForRadio3 <- google_analytics(ga_id,
                                      date_range = eight_month_date_range,
                                      metrics = "users",
                                      dimensions = "userGender",
                                      filtersExpression = "ga:eventLabel==CILM-FM",
                                      slow_fetch = TRUE)

# With anti-smaple, longer, will fetch all rows (6 min 30 s)
# I got a 503 error, due to complicated queries (i.e. lots of segments or dimensions) or too big queries
# 2 times retry
# Avoided sampling
gendereachForRadio4 <- google_analytics(ga_id,
                                      date_range = eight_month_date_range,
                                      metrics = "users",
                                      dimensions = "userGender",
                                      filtersExpression = "ga:eventLabel==CILM-FM",
                                      anti_sample = TRUE)

##### userAge
# Default (2 s)
# Data is sampled 
ageReachForRadio2 <- google_analytics(ga_id,
                                      date_range = eight_month_date_range,
                                      metrics = "users",
                                      dimensions = "userAgeBracket",
                                      filtersExpression = "ga:eventLabel==CILM-FM")


# With slow fetch (45 s)
# By default, 50,000 rows will be requested per API call. (10 000 rows x 5 calls)
# Reset to 10 000 rows per call
ageReachForRadio3 <- google_analytics(ga_id,
                                      date_range = eight_month_date_range,
                                      metrics = "users",
                                      dimensions = "userAgeBracket",
                                      filtersExpression = "ga:eventLabel==CILM-FM",
                                      slow_fetch = TRUE)

# With anti-smaple, longer, will fetch all rows (6 min 30 s)
# I got a 503 error, due to complicated queries (i.e. lots of segments or dimensions) or too big queries
# 2 times retry
# Avoided sampling
ageReachForRadio4 <- google_analytics(ga_id,
                                      date_range = eight_month_date_range,
                                      metrics = "users",
                                      dimensions = "userAgeBracket",
                                      filtersExpression = "ga:eventLabel==CILM-FM",
                                      anti_sample = TRUE)

##### coordinatesReach
# Default (25 s), more data
# Data is simpled 
coordinatesReach <- google_analytics(ga_id,
                                         date_range = eight_month_date_range,
                                         metrics = "users",
                                         dimensions = c("latitude", "longitude"),
                                         filtersExpression = "ga:eventLabel==CILM-FM")


# With slow fetch (2 s)
# By default, 50,000 rows will be requested per API call. (10 000 rows x 5 calls)
# Reset to 10 000 rows per call
coordinatesReach1 <- google_analytics(ga_id,
                                         date_range = eight_month_date_range,
                                         metrics = "users",
                                         dimensions = c("latitude", "longitude"),
                                         filtersExpression = "ga:eventLabel==CILM-FM",
                                         slow_fetch = TRUE)

# With anti-smaple, longer, will fetch all rows (6 min 30 s)
# I got a 503 error, due to complicated queries (i.e. lots of segments or dimensions) or too big queries
# 2 times retry
# Avoided sampling
coordinatesReach2 <- google_analytics(ga_id,
                                        date_range = eight_month_date_range,
                                        metrics = "users",
                                        dimensions = c("latitude", "longitude"),
                                        filtersExpression = "ga:eventLabel==CILM-FM",
                                        anti_sample = TRUE)
    
# If you are using anti-sampling, it will always fetch all rows. 
# This is because it won’t make sense to fetch only the top results as the API splits up the calls over all days. 
# In data analysis, sampling is the practice of analyzing a subset of all data in order 
# to uncover the meaningful information in the larger data set. 
# For example, if you wanted to estimate the number of trees in a 100-acre area 
# where the distribution of trees was fairly uniform, you could count the number of trees in 1 acre and multiply by 100, 
# or count the trees in a half acre and multiply by 200 to get an accurate representation of the entire 100 acres.

# By default the function will return 1000 results (rows). If you want to fetch all rows, set max = -1

## Next step: Write a script to automate this.

# By default, 1000 rows max, we can use option max = -1 for unlimited

# Fetch by multiple views : The limiting factor will then be the v4 API limits, 
# which are currently 1000 requests per 100 seconds per user, 
# per Google API project (e.g. you will probably want to setup your own Google project if making a lot of requests)

## Fetching from multiple views
## Caching -> local caching
## Rows per call